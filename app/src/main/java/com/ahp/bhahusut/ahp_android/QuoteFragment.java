package com.ahp.bhahusut.ahp_android;

import android.app.Activity;
import android.content.pm.ActivityInfo;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import org.apache.http.HttpResponse;
import org.json.JSONObject;

import java.util.ArrayList;

public class QuoteFragment extends Fragment {
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";

    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;

    private OnFragmentInteractionListener mListener;
    private ArrayList<Quote> fetchedQuotes = new ArrayList<Quote>();
    private ImageView nextButt;
    private ImageView backButt;
    private ImageView authorImg;


    private int fetchedQuoteIndex = -1;
    private ImageButton rememberButt;
    private ImageButton clearButt;
    private TextView content;
    private boolean remember = true;
    private String prev_date="";
    public String author_name = "Zen";



    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @return A new instance of fragment QuoteFragment.
     */
    // TODO: Rename and change types and number of parameters
    public static QuoteFragment newInstance(String param1) {
        QuoteFragment fragment = new QuoteFragment();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        fragment.setArguments(args);
        return fragment;
    }

    public QuoteFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            author_name = getArguments().getString(ARG_PARAM1);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_quote, container, false);


        nextButt = (ImageView)view.findViewById(R.id.imageView2);
        backButt = (ImageView)view.findViewById(R.id.imageView1);
        authorImg = (ImageView)view.findViewById(R.id.authorIMGV);
        content = (TextView)view.findViewById(R.id.textView1);


        nextButt.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
               //get quote from server by remember
               updateNextQuoteWithRemember();

            }
        });

        backButt.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {

               updatePrevQuoteWithRemember();
            }
        });

        //load next quote
        //get quote from server by remember
        updateNextQuoteWithRemember();

        //swipe event

        FrameLayout fl = (FrameLayout)view.findViewById(R.id.f_layout_quote_frag);

        new SwipeDetector(fl)
            .setOnSwipeListener(new SwipeDetector.onSwipeEvent() {

                @Override
                public void SwipeEventDetected(View v, SwipeDetector.SwipeTypeEnum swipeType) {
                    if (swipeType==SwipeDetector.SwipeTypeEnum.LEFT_TO_RIGHT) {
                        Log.e("sw","to right");
                        updatePrevQuoteWithRemember();
                    } else if (swipeType==SwipeDetector.SwipeTypeEnum.RIGHT_TO_LEFT) {
                        Log.e("sw","to left");
                        updateNextQuoteWithRemember();
                    }
                }
        });

        //get longest screen size in pixel
        int wpx = getResources().getDisplayMetrics().widthPixels;
        int hpx = getResources().getDisplayMetrics().heightPixels;
        int tpx;
        if (wpx > hpx)
            tpx = wpx;
        else
            tpx = hpx;

        Log.e("tpx",tpx+"");

        if (tpx <= 800) {
            LinearLayout.LayoutParams parms = new LinearLayout.LayoutParams(90,90);
            parms.gravity = Gravity.CENTER;
            parms.topMargin = 10;
            authorImg.setLayoutParams(parms);

            content.setTextSize(14);

            getActivity().setRequestedOrientation(
                    ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);

        }

        return view;
    }

    // TODO: Rename method, update argument and hook method into UI event
    public void onButtonPressed(Uri uri) {
        if (mListener != null) {
            mListener.onFragmentInteraction(uri);
        }
    }

    public class UpdateNextQuoteWithRemember extends Network.HttpConnect {
        public void onConnectionError() {
            MainActivity.act.runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    MainActivity.showStatus("Internet connection error!!");
                }
            });
        }

        public void onFinished() {

            if (!jsonString.isEmpty()) {
                try {
                    JSONObject jo = new JSONObject(jsonString);
                    content.setText(jo.getString("content"));

                    //update author images
                    for (int i = 0; i < MainActivity.authorDetail.size(); i++) {
                        String authorId = MainActivity.authorDetail.get(i).id;
                        Bitmap authorBMP = MainActivity.authorDetail.get(i).img;
                        if (jo.getString("author_id").equals(authorId))
                            authorImg.setImageBitmap(authorBMP);
                    }

                    fetchedQuotes.add(new Quote(
                            jo.getString("_id"),
                            jo.getString("content"),
                            jo.getString("author_id"),
                            jo.getString("date")
                    ));

                    fetchedQuoteIndex++;
                } catch (Exception e) {
                    Log.e("UNQORemem", e.getMessage());
                }
            } else {
                MainActivity.showStatus("There is no next quote for this author.");
            }
        }
    }

    public class UpdatePrevQuoteWithRemember extends Network.HttpConnect {
        public void onConnectionError() {
            MainActivity.act.runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    MainActivity.showStatus("Internet connection error!!");
                }
            });
        }

        public void onFinished() {

            if (!jsonString.isEmpty()) {
                try {
                    JSONObject jo = new JSONObject(jsonString);
                    content.setText(jo.getString("content"));

                    //update author images
                    for (int i = 0; i < MainActivity.authorDetail.size(); i++) {
                        String authorId = MainActivity.authorDetail.get(i).id;
                        Bitmap authorBMP = MainActivity.authorDetail.get(i).img;
                        if (jo.getString("author_id").equals(authorId))
                            authorImg.setImageBitmap(authorBMP);
                    }

                    fetchedQuotes.add(new Quote(
                            jo.getString("_id"),
                            jo.getString("content"),
                            jo.getString("author_id"),
                            jo.getString("date")
                    ));

                    fetchedQuoteIndex++;
                } catch (Exception e) {
                    Log.e("UNQORemem", e.getMessage());
                }
            } else {
                MainActivity.showStatus("There is no previous quote for this author.");
            }
        }
    }

    public void updateNextQuoteWithRemember() {
        try {
            HttpResponse res = null;
            String jstr = "";
            jstr = new UpdateNextQuoteWithRemember()
                    .execute(Network.api_host + "/get_next_quote",
                            "fb_id", MainActivity.fb_id,
                            "authorname", author_name).get();


        } catch (Exception e) {
            Log.e("http get_next_quote", e.getMessage());
        }
    }

    public void updatePrevQuoteWithRemember() {
        try {
            HttpResponse res = null;
            String jstr = "";
            jstr = new UpdatePrevQuoteWithRemember()
                    .execute(Network.api_host + "/get_prev_quote",
                            "fb_id", MainActivity.fb_id,
                            "authorname", author_name).get();


        } catch (Exception e) {
            Log.e("http get_next_quote", e.getMessage());
        }
    }


    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);

        /*
        try {
            mListener = (OnFragmentInteractionListener) activity;
        } catch (ClassCastException e) {
            throw new ClassCastException(activity.toString()
                    + " must implement OnFragmentInteractionListener");
        }*/
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }


    public interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        public void onFragmentInteraction(Uri uri);
    }

    public class Quote {
        String id;
        String content;
        String author_id;
        String date;

        Quote(String _id,String _content,String _author_id,String _date) {
            id = _id;
            content = _content;
            author_id = _author_id;
            date = _date;
        }
    }



}

