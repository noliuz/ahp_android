package com.ahp.bhahusut.ahp_android;

import android.content.Context;
import android.content.res.AssetManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTabHost;
import android.support.v7.app.ActionBar;
import android.support.v7.app.ActionBarActivity;
import android.util.Log;
import android.view.KeyEvent;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.inputmethod.EditorInfo;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TabHost;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdView;

import org.apache.http.HttpResponse;
import org.json.JSONArray;
import org.json.JSONObject;

import java.io.InputStream;
import java.util.ArrayList;


public class MainActivity extends ActionBarActivity {

    //public static ArrayList<AuthorImage> authorImages = new ArrayList<AuthorImage>();
    public static FragmentTabHost mTabHost=null;
    EditText searchET;
    FragmentManager fm=null;
    Button clearBT;
    public static QuoteContainerFragment qcFrag = null;
    public static QuoteSearchedResultFragment qsrFrag = null;
    public static boolean isLoadAuthor = false;
    public static ArrayList<AuthorListFragment.Author> authorDetail
        = new ArrayList<AuthorListFragment.Author>();
    public static boolean hideIntroText = false;
    public static View static_view = null;
    public static MainActivity act = null;
    public static boolean wasLoadAuthors = false;
    public static String fb_id="111";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        act = this;

        //admob ads
        AdView mAdView = (AdView) findViewById(R.id.adView);
        AdRequest adRequest = new AdRequest.Builder()
                .addTestDevice("69C38311626A1F7FE6E5BA489C16B249")
                .addTestDevice("210F2411236CD23A76264EA9FD5758C7")
                .addTestDevice("76E133D375A61BF9D61CC306431B597E")
                .build();
        mAdView.loadAd(adRequest);

        //tabs
        mTabHost = (FragmentTabHost)findViewById(android.R.id.tabhost);
        mTabHost.setup(this, getSupportFragmentManager(), android.R.id.tabcontent);

        mTabHost.addTab(
                mTabHost.newTabSpec("quote_container").setIndicator("Quotes", null),
                QuoteContainerFragment.class, null);
        mTabHost.addTab(
                mTabHost.newTabSpec("authors").setIndicator("Authors", null),
                AuthorListFragment.class, null);

        mTabHost.setOnTabChangedListener(new TabHost.OnTabChangeListener() {
            @Override
            public void onTabChanged(String tabId) {
               // Log.e("d",tabId);
                //if (tabId.equals(("quote_container"))) {
                  //  Log.e("d","tab change 0");
                //}
            }
        });

        //fetch author images
        //Network.fetchAuthorImages();

        //debug
        //updateAuthorsFromServer();

        //add search edittext to actionbar
        ActionBar actionBar = getSupportActionBar();
        actionBar.setCustomView(R.layout.search_layout);
        actionBar.setDisplayOptions(ActionBar.DISPLAY_SHOW_CUSTOM
                | ActionBar.DISPLAY_SHOW_HOME);



        //get search edittext
        searchET = (EditText)actionBar.getCustomView().findViewById(R.id.txt_search);
        searchET.setText("");
        searchET.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                if (actionId == EditorInfo.IME_ACTION_DONE) {
                    if (mTabHost.getCurrentTab() == 0) {
                        quoteTabSearchHit();
                    } else if (mTabHost.getCurrentTab() == 1) {
                        authorTabSearchHit();
                    }
                }
                return false;
            }
        });

        //get clear button of edittext
        clearBT = (Button)actionBar.getCustomView().findViewById(R.id.txt_clear);
        clearBT.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View view) {
                searchET.setText("");

                //Author fragment active
                AuthorListFragment alf =
                        (AuthorListFragment)getSupportFragmentManager().findFragmentByTag("authors");
                if (alf != null && alf.isVisible()) {
                    alf.filterAuthors("");
                }

                //Quote search fragment active
                if (qsrFrag != null && qsrFrag.isVisible()) {
                    qsrFrag.clearResult();
                }

            }
        });

        mTabHost.setCurrentTab(0);

        //get fb_id from bunble
        Bundle b = getIntent().getExtras();
        if (b != null) {
            fb_id = b.getString("fb_id");
        }
        Log.e("fb_id",fb_id);


    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);

        updateAuthorsFromServer();

        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        } else if (id== R.id.action_search) {
            if (mTabHost.getCurrentTab() == 1) {//if Author tab active
                authorTabSearchHit();
            } else if (mTabHost.getCurrentTab() == 0) {//if Quotes tab active
                quoteTabSearchHit();
            }
        }

        return super.onOptionsItemSelected(item);
    }

    public void quoteTabSearchHit() {
        InputMethodManager imm = (InputMethodManager)getSystemService(
                Context.INPUT_METHOD_SERVICE);
        imm.hideSoftInputFromWindow(searchET.getWindowToken(), 0);

        if (searchET.getText().toString().length() < 3) {
            showStatus("Keyword must be more than 3 characters");
        } else {
            qcFrag.replaceFragment(qsrFrag,false,"quote_search");
            //qsrFrag.updateQuoteResults(searchET.getText().toString());
            qsrFrag.updateSearchedQuotesFromServer(searchET.getText().toString());
        }
    }

    public void authorTabSearchHit() {
        AuthorListFragment alf =
                (AuthorListFragment)getSupportFragmentManager().findFragmentByTag("authors");

        InputMethodManager imm = (InputMethodManager)getSystemService(
                Context.INPUT_METHOD_SERVICE);
        imm.hideSoftInputFromWindow(searchET.getWindowToken(), 0);

        //Log.e("xx",searchET.getText().toString());
        alf.filterAuthors(searchET.getText().toString());
    }

    public Bitmap convertTextFile2Bitmap(String asset_fname) {
        //convert String to image

        AssetManager assetMan = getAssets();
        Bitmap bmp1=null;
        try {
            InputStream isr = assetMan.open(asset_fname);
            bmp1 = BitmapFactory.decodeStream(isr);
        } catch (Exception e) {
            Log.e("Asset", "open asset error" + e.getMessage());
        }

        return bmp1;
    }

    public static void showStatus(String str) {
        Toast.makeText(act,str,Toast.LENGTH_LONG).show();
    }


    /*
    public static void showLoadingPanel(boolean status) {
        if (status == false)
            act.getWindow().getDecorView()
                .findViewById(R.id.loadingPanel).setVisibility(View.GONE);
        else
            act.getWindow().getDecorView()
                    .findViewById(R.id.loadingPanel).setVisibility(View.VISIBLE);
    }*/

    public void updateAuthorsFromServer() {
        //get author from server
        Log.e("d","updateAuthorsFromServer");

        try {
            HttpResponse res = null;
            //String jstr = new Network.LoadAuthorsDetail().execute(Network.api_host + "/get_author").get();
            new LoadAuthorFromServer()
                    .execute(Network.api_host + "/get_author");

        } catch (Exception e) {
            Log.e("HttpConnect",e.getStackTrace().toString());
        }

    }

    public class LoadAuthorFromServer extends Network.HttpConnect {

        public void onConnectionError() {
            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    MainActivity.showStatus("Internet connection error!!");
                }
            });
        }

        public void onFinished() {
            MainActivity.wasLoadAuthors = true;

            try {
                //parse JSON response
                JSONArray ja = new JSONArray(jsonString);

                for (int i = 0; i < ja.length(); i++) {
                    JSONObject jo = ja.getJSONObject(i);
                    //JSONstr img
                    String imgJsonStr = jo.getString("img");
                    String imgStr = new JSONObject(imgJsonStr).getString("data");
                    imgStr = new JSONObject(imgStr).getString("data");
                    imgStr = imgStr.substring(1, imgStr.length() - 1);//cut brackets

                    //convert imgStr to byte array
                    //Log.e("r",imgJsonStr);
                    String[] imgStrA = imgStr.split(",");
                    byte[] imgByteA = new byte[imgStrA.length];
                    for (int j = 0; j < imgStrA.length; j++)
                        imgByteA[j] = Network.convertString2Byte(imgStrA[j]);
                    //Log.e("r","2");

                    //add author
                    Bitmap tmpbmp = BitmapFactory.decodeByteArray(imgByteA, 0
                            , imgByteA.length);
                    if (tmpbmp == null) {
                        Log.e("image decode", "error: image can not decode");
                    }

                    AuthorListFragment.Author a = new
                            AuthorListFragment.Author(jo.getString("_id"), jo.getString("name"),
                            jo.getString("bio"), tmpbmp);

                    MainActivity.authorDetail.add(a);

                    //add author to UI array

                    AuthorListFragment.AuthorInfo ai = new AuthorListFragment.AuthorInfo();
                    ai.name = a.name;
                    ai.bio = a.bio;
                    ai.bmp = tmpbmp;

                    AuthorListFragment.authorListUI.add(ai);
                }
            } catch (Exception e) {
                Log.e("LoadAuthorFromServer",e.getMessage());
            }
        }
    }
}