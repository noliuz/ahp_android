package com.ahp.bhahusut.ahp_android;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.util.Log;

/**
 * Created by nol on 18/6/2558.
 */
public class BaseContainerFragment extends Fragment {
    public void replaceFragment(Fragment fragment, boolean addToBackStack,String tag) {

            FragmentTransaction transaction = getChildFragmentManager().beginTransaction();
            if (addToBackStack) {
                transaction.addToBackStack(null);
            }
            transaction.replace(R.id.container_framelayout, fragment, tag);
            transaction.commitAllowingStateLoss();
            getChildFragmentManager().executePendingTransactions();

        /*
        if (getChildFragmentManager().findFragmentByTag("quote_container") == null)
            Log.e("d","q null");
        else
            Log.e("d","q not null");*/
    }

    public boolean popFragment() {
        Log.e("test", "pop fragment: " + getChildFragmentManager().getBackStackEntryCount());
        boolean isPop = false;
        if (getChildFragmentManager().getBackStackEntryCount() > 0) {
            isPop = true;
            getChildFragmentManager().popBackStack();
        }
        return isPop;
    }
}
