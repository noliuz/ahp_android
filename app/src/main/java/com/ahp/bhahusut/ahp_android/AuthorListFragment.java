package com.ahp.bhahusut.ahp_android;

import android.app.Activity;
import android.content.res.AssetManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import java.io.ByteArrayOutputStream;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;

public class AuthorListFragment extends Fragment {
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";
    public  static final String ARG_OBJ = "obj";


    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;

    private OnFragmentInteractionListener mListener;
    private RecyclerView rView;
    private RecyclerView.LayoutManager rlManager;
    private RecyclerView.Adapter rAdapter;
    List<AuthorInfo> cl=null;
    boolean updatedAuthors = false;

    //init arraylist
    public static ArrayList<AuthorInfo> authorListUI = new ArrayList<AuthorInfo>();



    // TODO: Rename and change types and number of parameters
    public static AuthorListFragment newInstance(String param1, String param2) {
        AuthorListFragment fragment = new AuthorListFragment();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    public AuthorListFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }

    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.fragment_author_list2, container, false);

        //recyclerView
        rView = (RecyclerView)view.findViewById(R.id.my_recycler_view);
        rView.setHasFixedSize(true);

        rlManager = new LinearLayoutManager(this.getActivity());
        rView.setLayoutManager(rlManager);

        //debug
        /*
        if (!MainActivity.isLoadAuthor) {
            updateAuthorsFromServer();

            MainActivity.isLoadAuthor = true;
        }*/

        //display author to UI
        rAdapter = new ContactAdapter(authorListUI);
        rView.setAdapter(rAdapter);

        MainActivity.hideIntroText = true;

        return view;
    }

    // TODO: Rename method, update argument and hook method into UI event
    public void onButtonPressed(Uri uri) {
        if (mListener != null) {
            mListener.onFragmentInteraction(uri);
        }
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        /*
        try {
            mListener = (OnFragmentInteractionListener) activity;
        } catch (ClassCastException e) {
            throw new ClassCastException(activity.toString()
                    + " must implement OnFragmentInteractionListener");
        }*/
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }


    public interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        public void onFragmentInteraction(Uri uri);
    }


    public class ContactAdapter extends RecyclerView.Adapter<ContactAdapter.ContactViewHolder> {

        private List<AuthorInfo> contactList;

        public ContactAdapter(List<AuthorInfo> contactList) {
            this.contactList = contactList;
        }

        @Override
        public int getItemCount() {
            return contactList.size();
        }

        @Override
        public void onBindViewHolder(ContactViewHolder contactViewHolder, int i) {
            AuthorInfo ci = contactList.get(i);
            contactViewHolder.vName.setText(ci.name);
            contactViewHolder.vBio.setText(ci.bio);
            contactViewHolder.vImg.setImageBitmap(ci.bmp);
        }

        @Override
        public ContactViewHolder onCreateViewHolder(ViewGroup viewGroup, int i) {
            View itemView = LayoutInflater.
                    from(viewGroup.getContext()).
                    inflate(R.layout.author_card, viewGroup, false);

            return new ContactViewHolder(itemView);
        }

        public class ContactViewHolder extends RecyclerView.ViewHolder {
            public TextView vName;
            public TextView vBio;
            public ImageView vImg;

            public ContactViewHolder(View v) {
                super(v);
                vName =  (TextView) v.findViewById(R.id.txtName);
                vBio = (TextView)  v.findViewById(R.id.txtBio);
                vImg = (ImageView)v.findViewById(R.id.author_image);

                //cardview clicked
                v.setOnClickListener(new OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        //Log.e("RecyclerView", "onClick：" + authors.get(getPosition()).name);
                        String author_name = MainActivity.authorDetail.get(getPosition()).name;

                        //debug
                        /*
                        if (MainActivity.qsrFrag == null)
                            Log.e("d","author qcf null");
                        else
                            Log.e("d","author qcf not null");
                        */



                        ((BaseContainerFragment)MainActivity.qsrFrag.getParentFragment())
                            .replaceFragment(new QuoteFragment().newInstance(author_name)
                                    , false, "quote_show");
                        MainActivity.mTabHost.setCurrentTab(0);


                    }
                });
            }


        }


    }

    public static class AuthorInfo {
        protected String name;
        protected String bio;
        protected Bitmap bmp;

        AuthorInfo() {

        }
    }



    public static class Author {
        String id;
        String name;
        String bio;
        //byte[] imgByteA;
        Bitmap img;

        Author(String _id,String _name,String _bio,Bitmap _img) {
            id = _id;
            name = _name;
            bio = _bio;
            img = _img;
        }
    }

    //debug
    public void getImageFromFile() {//debug
        //debug: get image from file
        AssetManager assetMan = getActivity().getAssets();
        Bitmap bmp1=null;
        byte[] tmpBA=null;

        try {
            InputStream isr = assetMan.open("tmp.txt");
            bmp1 = BitmapFactory.decodeStream(isr);


            //debug
            isr = assetMan.open("tmp.txt");
            ByteArrayOutputStream buf = new ByteArrayOutputStream();
            int nRead;
            byte[] data = new byte[16384];
            while ((nRead = isr.read(data, 0, data.length)) != -1) {
                buf.write(data, 0, nRead);
            }
            buf.flush();

            tmpBA = buf.toByteArray();

        } catch (Exception e) {
            Log.e("Asset", "open asset error" + e.getMessage());
        }
    }



    public void filterAuthors(String keyword) {
        List<AuthorInfo> cl1 = new ArrayList<AuthorInfo>();
        for (int i=0;i<authorListUI.size();i++) {
            if (authorListUI.get(i)
                    .name.toLowerCase().contains(keyword.toLowerCase()) ||
                    authorListUI.get(i)
                            .bio.toLowerCase().contains(keyword.toLowerCase()) ) {
                cl1.add(authorListUI.get(i));
            }
        }

        if (cl1.size() == 0) {
            showStatus("There is not any author match");
        }

        rAdapter = new ContactAdapter(cl1);
        rView.setAdapter(rAdapter);
    }

    public void showStatus(String str) {
        Toast.makeText(getActivity(), str, Toast.LENGTH_LONG).show();
    }

}
