package com.ahp.bhahusut.ahp_android;

import android.graphics.Bitmap;

/**
 * Created by nol on 17/4/2558.
 */
public class AuthorImage {
    public Bitmap bmp=null;
    public String id;

    AuthorImage(String _id,Bitmap _bmp) {
        bmp = _bmp;
        id = _id;

    }
}
