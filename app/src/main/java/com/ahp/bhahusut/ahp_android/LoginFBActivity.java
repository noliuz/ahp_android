package com.ahp.bhahusut.ahp_android;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;

import com.facebook.AccessToken;
import com.facebook.CallbackManager;
import com.facebook.FacebookCallback;
import com.facebook.FacebookException;
import com.facebook.FacebookSdk;
import com.facebook.appevents.AppEventsLogger;
import com.facebook.login.LoginResult;
import com.facebook.login.widget.LoginButton;

public class LoginFBActivity extends ActionBarActivity {

    LoginButton loginBT;
    CallbackManager callbackManager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        //init facebook sdk
        FacebookSdk.sdkInitialize(getApplicationContext());
        callbackManager = CallbackManager.Factory.create();

        setContentView(R.layout.activity_login);

        loginBT = (LoginButton) findViewById(R.id.login_button);


        // loginBT.setLoginBehavior(LoginBehavior.SUPPRESS_SSO);
        loginBT.registerCallback(callbackManager, new FacebookCallback<LoginResult>() {
            @Override
            public void onSuccess(LoginResult loginResult) {
                Log.e("fb_success", loginResult.getAccessToken().getUserId());

                openQuotesActivity(loginResult.getAccessToken().getUserId());
            }

            @Override
            public void onCancel() {
                // App code
                Log.e("fb_login", "cancel");
            }

            @Override
            public void onError(FacebookException exception) {
                // App code
                Log.e("fb_login", exception.getMessage());
            }
        });

        //CookieManager cookieManager = CookieManager.getInstance();
        //cookieManager.setAcceptCookie(true);

        //check if logged
        AccessToken at = AccessToken.getCurrentAccessToken();
        if (at == null) {
            Log.e("log","not log");
        } else {
            openQuotesActivity(at.getUserId());
        }

/*
        try {

            PackageInfo info = getPackageManager().getPackageInfo(
                    "com.ahp.bhahusut.ahp_android",
                    PackageManager.GET_SIGNATURES);
            for (Signature signature : info.signatures) {
                MessageDigest md = MessageDigest.getInstance("SHA");
                md.update(signature.toByteArray());
                Log.e("KeyHash:", Base64.encodeToString(md.digest(), Base64.DEFAULT));
            }
        } catch (Exception e) {
            Log.w("hash",e.getMessage());
        }
*/
    }

    public void openQuotesActivity(String fb_id) {
        Intent intent = new Intent(this, MainActivity.class);
        Bundle b = new Bundle();
        b.putString("fb_id", fb_id); //Your id
        intent.putExtras(b); //Put your id to your next Intent
        startActivity(intent);
        finish();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        //fb
        callbackManager.onActivityResult(requestCode, resultCode, data);


    }

    @Override
    protected void onResume() {
        super.onResume();
        AppEventsLogger.activateApp(this);
    }

    @Override
    protected void onPause() {
        super.onPause();
        AppEventsLogger.deactivateApp(this);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_login, menu);

        setTitle("Facebook Login");
        //getActionBar().setIcon(R.drawable.ahp_logo);

        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
}
