package com.ahp.bhahusut.ahp_android;

import android.accounts.Account;
import android.accounts.AccountManager;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.Toast;

import com.facebook.CallbackManager;
import com.facebook.FacebookSdk;
import com.facebook.login.widget.LoginButton;

public class LoginGoogleActivity extends ActionBarActivity {

    LoginButton loginBT;
    CallbackManager callbackManager;
    AccountManager am;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        //init facebook sdk
        FacebookSdk.sdkInitialize(getApplicationContext());
        callbackManager = CallbackManager.Factory.create();

        setContentView(R.layout.activity_login_google);

        //Google account
        String name = "";
        am = AccountManager.get(getApplicationContext());
        Account[] accounts = AccountManager.get(getApplicationContext())
                .getAccountsByType("com.google");
        if (accounts.length > 0) {
            name = accounts[0].name;
            openQuotesActivity(name);
        } else {
            showStatus("Please add google account before use this app.");
        }

    }

    public void showStatus(String str) {
        Toast.makeText(this, str, Toast.LENGTH_LONG).show();
    }

    public void openQuotesActivity(String fb_id) {
        Intent intent = new Intent(this, MainActivity.class);
        Bundle b = new Bundle();
        b.putString("fb_id", fb_id); //Your id
        intent.putExtras(b); //Put your id to your next Intent
        startActivity(intent);
        finish();
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_login, menu);

        setTitle("Google Login");

        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
}
