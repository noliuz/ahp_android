package com.ahp.bhahusut.ahp_android;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

/**
 * Created by nol on 18/6/2558.
 */
public class QuoteContainerFragment extends BaseContainerFragment {
    private boolean mIsViewInited=false;


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        //Log.e("test", "tab 1 oncreateview");

        return inflater.inflate(R.layout.container_fragment, null);
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        //Log.e("test", "tab 1 container on activity created");

        if (!mIsViewInited) {
            mIsViewInited = true;

            MainActivity.qcFrag =
                  (QuoteContainerFragment)getActivity().getSupportFragmentManager()
                        .findFragmentByTag("quote_container");

            //debug
            /*
            if (MainActivity.qcFrag == null)
                Log.e("d", "qc null");
            else
                Log.e("d","qc not null");*/

            replaceFragment(new QuoteSearchedResultFragment(), false,"quote_search");


        }

    }



}
