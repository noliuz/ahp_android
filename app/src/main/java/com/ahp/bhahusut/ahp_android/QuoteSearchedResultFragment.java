package com.ahp.bhahusut.ahp_android;

import android.app.Activity;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;


/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link QuoteSearchedResultFragment.OnFragmentInteractionListener} interface
 * to handle interaction events.
 * Use the {@link QuoteSearchedResultFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class QuoteSearchedResultFragment extends Fragment {
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";

    private RecyclerView rView;
    private RecyclerView.LayoutManager rlManager;
    private RecyclerView.Adapter rAdapter;
    private List<QuoteInfo> ql;
    private ArrayList<QuoteSearch> quotes = new ArrayList<QuoteSearch>();
    //debug
    private String keyword="";


    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;

    private OnFragmentInteractionListener mListener;
    public static ArrayList<AuthorListFragment.AuthorInfo> authorListUI = null;
    static TextView tv1 = null;

    // TODO: Rename and change types and number of parameters
    public static QuoteSearchedResultFragment newInstance(String param1, String param2) {
        QuoteSearchedResultFragment fragment = new QuoteSearchedResultFragment();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    public QuoteSearchedResultFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        MainActivity.qsrFrag = (QuoteSearchedResultFragment)
                MainActivity.qcFrag.getChildFragmentManager().findFragmentByTag("quote_search");

        //debug
        /*
        if (MainActivity.qsrFrag == null)
            Log.e("d", "qsr1 null");
        else
            Log.e("d","qsr1 not null");
        */

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        View v = inflater.inflate(R.layout.fragment_quote_searched_result, container, false);
        rView = (RecyclerView)v.findViewById(R.id.qsrRecyclerView);

        rView.setHasFixedSize(true);

        rlManager = new LinearLayoutManager(this.getActivity());
        rView.setLayoutManager(rlManager);

        ql = new ArrayList<QuoteInfo>();
        if (!keyword.isEmpty())
            updateQuoteResults(keyword);

        //display quote to UI
        rAdapter = new QuoteAdapter(ql);
        rView.setAdapter(rAdapter);

        //views
        tv1 = (TextView)v.findViewById(R.id.textView1);

        hideIntroText();

        return v;
    }

    public void showStatus(String str) {
        Toast.makeText(getActivity(), str, Toast.LENGTH_LONG).show();
    }

    public static void hideIntroText() {
        if (MainActivity.hideIntroText == true) {
            tv1.setVisibility(View.INVISIBLE);
        }

    }

    public void clearResult() {
        ql = new ArrayList<QuoteInfo>();

        //display quote to UI
        rAdapter = new QuoteAdapter(ql);
        rView.setAdapter(rAdapter);

    }

    public void updateQuoteResults(String key) {
        /*
        keyword = key;
        ql = new ArrayList<QuoteInfo>();
        updateSearchedQuotesFromServer();

        if (ql.size() == 0) {
            showStatus("There is no any quote match");
        }

        //display quote to UI
        rAdapter = new QuoteAdapter(ql);
        rView.setAdapter(rAdapter);

        //hide intro text
        MainActivity.hideIntroText = true;
        this.hideIntroText();*/
    }

    public void updateSearchedQuotesFromServer(String key) {
        quotes.clear();
        //get searched quotes from server
        try {

            String jstr
                = new UpdateSearchedQuoteFromServer()
                    .execute(Network.api_host + "/search_quote",
                    "fb_token","adslkfj204758","keyword",key).get();
            //String jstr = EntityUtils.toString(res.getEntity());


        } catch (Exception e) {
            Log.e("HttpConnect", e.getMessage());
        }
    }

    // TODO: Rename method, update argument and hook method into UI event
    public void onButtonPressed(Uri uri) {
        if (mListener != null) {
            mListener.onFragmentInteraction(uri);
        }
    }

    public class UpdateSearchedQuoteFromServer extends Network.HttpConnect {
        public void onConnectionError() {
            MainActivity.act.runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    MainActivity.showStatus("Internet connection error!!");
                }
            });
        }

        public void onFinished() {
            try {

                //Log.e("D",res);

                //clear quotelist for UI
                if (ql == null)
                    ql = new ArrayList<QuoteInfo>();
                else
                    ql.clear();

                //parse JSON response
                JSONArray ja = new JSONArray(jsonString);
                for (int i=0;i<ja.length();i++) {
                    JSONObject jo = ja.getJSONObject(i);

                    //add Quote
                    QuoteSearch q = new QuoteSearch(jo.getString("_id"), jo.getString("content"),
                            jo.getString("author_id"));
                    quotes.add(q);

                    //add author to UI array
                    QuoteInfo qi = new QuoteInfo();
                    qi.content = q.content;
                    qi.author_id = q.author_id;

                    ql.add(qi);
                }
            } catch (Exception e) {
                Log.e("QSR",e.getMessage());
            }

            if (ql.size() == 0) {
                showStatus("There is no any quote match");
            }

            //display quote to UI
            rAdapter = new QuoteAdapter(ql);
            rView.setAdapter(rAdapter);

            //hide intro text
            MainActivity.hideIntroText = true;
            MainActivity.qsrFrag.hideIntroText();
        }
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);

        /*
        try {
            mListener = (OnFragmentInteractionListener) activity;
        } catch (ClassCastException e) {
            throw new ClassCastException(activity.toString()
                    + " must implement OnFragmentInteractionListener");
        }*/
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p/>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */
    public interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        public void onFragmentInteraction(Uri uri);
    }


    public class QuoteInfo {
        String content;
        String author_id;

    }

    public class QuoteAdapter extends RecyclerView.Adapter<QuoteAdapter.QuoteViewHolder> {

        private List<QuoteInfo> quoteList;

        public QuoteAdapter(List<QuoteInfo> quoteList) {
            this.quoteList = quoteList;
        }

        @Override
        public int getItemCount() {
            return quoteList.size();
        }

        @Override
        public void onBindViewHolder(QuoteViewHolder quoteViewHolder, int i) {
            QuoteInfo qi = quoteList.get(i);
            quoteViewHolder.vContent.setText(qi.content);
            //get image
            Bitmap authorBMP = null;
            for (int j=0;j<MainActivity.authorDetail.size();j++) {
                String aIID = MainActivity.authorDetail.get(j).id;

                Bitmap aBMP = MainActivity.authorDetail.get(j).img;
                if (aIID.equals(qi.author_id)) {
                    authorBMP = aBMP;
                }
            }
            quoteViewHolder.vImg.setImageBitmap(authorBMP);
        }

        @Override
        public QuoteViewHolder onCreateViewHolder(ViewGroup viewGroup, int i) {
            View itemView = LayoutInflater.
                    from(viewGroup.getContext()).
                    inflate(R.layout.quote_card, viewGroup, false);

            return new QuoteViewHolder(itemView);
        }

        public class QuoteViewHolder extends RecyclerView.ViewHolder {
            public TextView vContent;
            public ImageView vImg;

            public QuoteViewHolder(View v) {
                super(v);
                vContent =  (TextView) v.findViewById(R.id.quoteContentTV);
                vImg = (ImageView)v.findViewById(R.id.quoteAuthorIV);
            }
        }
    }

    class QuoteSearch {
        public String id;
        public String content;
        public String author_id;

        QuoteSearch (String _id,String _content,String _author_id) {
            id = _id;
            content = _content;
            author_id = _author_id;
        }
    }
}
