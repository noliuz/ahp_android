package com.ahp.bhahusut.ahp_android;

import android.app.ProgressDialog;
import android.os.AsyncTask;
import android.util.Log;

import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.conn.scheme.Scheme;
import org.apache.http.conn.scheme.SchemeRegistry;
import org.apache.http.conn.ssl.SSLSocketFactory;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.impl.conn.SingleClientConnManager;
import org.apache.http.params.BasicHttpParams;
import org.apache.http.params.HttpConnectionParams;
import org.apache.http.params.HttpParams;
import org.apache.http.util.EntityUtils;
import org.json.JSONObject;

import java.nio.charset.Charset;

/**
 * Created by nol on 15/4/2558.
 */
public class Network {

    //public static String api_host = "http://10.0.2.2:3000/api";
    //public static String api_host = "http://192.168.0.114:3000/api";
    //public static String api_host = "http://128.199.136.74:8085/api";
    //public static String api_host = "http://ahp.bhahusut.xyz:8085/api";
    public static String api_host = "https://ahp.bhahusut.xyz/api";

    //ex.
    //        .execute("http://10.0.2.2:3000","fb_token","2908570rf","author_name","Nol");
    public static abstract class HttpConnect extends AsyncTask<String,Integer,String> {
        ProgressDialog progress;
        String jsonString="";

        public abstract void onFinished();
        public abstract void onConnectionError();

        @Override
        protected void onPreExecute() {
            super.onPreExecute();

            progress = new ProgressDialog(MainActivity.act);
            progress.setMessage("Loading...");
            progress.setIndeterminate(false);
            progress.setProgressStyle(ProgressDialog.STYLE_SPINNER);
            progress.setCancelable(true);
            progress.show();
            //Log.e("HttpCon","was called");
        }

        private HttpResponse res=null;
        protected String doInBackground(String... urls) {
            try {

                //http post
                HttpParams httpParams = new BasicHttpParams();
                int timeoutConnection = 2000;
                HttpConnectionParams.setConnectionTimeout(httpParams,timeoutConnection);
                int timeoutSocket = 3000;
                HttpConnectionParams.setSoTimeout(httpParams,timeoutSocket);

                SchemeRegistry sr = new SchemeRegistry();
                sr.register(new Scheme("https",
                        SSLSocketFactory.getSocketFactory(), 443));
                SingleClientConnManager mgr = new SingleClientConnManager(httpParams,sr);

                HttpClient httpClient = new DefaultHttpClient(mgr,httpParams);

                HttpPost httpPost = new HttpPost(urls[0]);
                httpPost.setHeader("Content-Type","application/json");

                //parse params
                if (urls.length >= 3) {
                    JSONObject jo = new JSONObject();
                    int pair_count = (urls.length-1)/2;
                    for (int i = 0; i < pair_count; i++) {
                        int posI = i*2+1;
                        //Log.e("http",urls[posI]+" "+urls[posI+1]);
                        jo.put(urls[posI],urls[posI+1]);
                    }
                    httpPost.setEntity(new StringEntity(jo.toString(),"UTF-8"));
                }


                res = httpClient.execute(httpPost);


                if (res.getStatusLine().getStatusCode() == 200) {
                    jsonString = EntityUtils.toString(res.getEntity());
                }

                /*
                try {
                    Thread.sleep(3000);
                } catch (Exception e) {
                    Log.e("Thread",e.getMessage());
                }*/

            }  catch (Exception e) {
                //MainActivity.showStatus("There is problem with internet connection");

                onConnectionError();
                Log.e("http_connect", e.getMessage());
            }

            return null;
        }

        @Override
        protected void onPostExecute(String res) {
            super.onPostExecute(res);
            progress.dismiss();
            onFinished();

           // Log.e("HttpCon","Finished");
        }

    }



    /*
    public static void fetchAuthorImages() {
        try {
            String jstr = new HttpConnect()
                    .execute(api_host + "/get_author_images").get();

            //parse JSON response
            JSONArray ja = new JSONArray(jstr);
            for (int i=0;i<ja.length();i++) {
                JSONObject jo = ja.getJSONObject(i);
                //JSONstr img
                String imgJsonStr = jo.getString("img");
                String imgStr  = new JSONObject(imgJsonStr).getString("data");
                imgStr = imgStr.substring(1,imgStr.length()-1);//cut brackets

                //convert imgStr to byte array
                String[] imgStrA= imgStr.split(",");
                byte[] imgByteA = new byte[imgStrA.length];
                for (int j=0;j<imgStrA.length;j++)
                    imgByteA[j] = convertString2Byte(imgStrA[j]);

                //add author images
                Bitmap tmpbmp = BitmapFactory.decodeByteArray(imgByteA, 0
                        , imgByteA.length);
                if (tmpbmp == null) {
                    Log.e("image decode","error: image can not decode");
                }

            }
        } catch (Exception e) {
            Log.e("fetchAuthorImage",e.getMessage());
        }

    }*/

    public static byte convertString2Byte(String str) {
        //ex. "200" == 200(byte)
        byte[] tmpA = str.getBytes(Charset.defaultCharset());

        //Log.e("str2byte",(tmpA[0]-48)+" "+(tmpA[1]-48)+" "+(tmpA[2]-48));
        if (tmpA.length == 1)
            return (byte)(tmpA[0]-48);
        if (tmpA.length == 2)
            return (byte)((tmpA[0]-48)*10+(tmpA[1]-48));
        else {
            return (byte) (((tmpA[0]-48)*100) + ((tmpA[1]-48) * 10) + (tmpA[2]-48));
        }
    }


}
